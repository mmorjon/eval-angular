import { HttpClient, HttpParams } from "@angular/common/http";
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import { map, tap } from "rxjs";

/**
 * Leaflet Map import section
 */
import * as L from "leaflet";
import { Router } from '@angular/router';


@Component({
  selector: 'app-listitems',
  templateUrl: './listitems.component.html',
  styleUrls: ['./listitems.component.scss']
})
export class ListitemsComponent implements OnInit, AfterViewInit {
  detailedCenter;
  centers;
  totalCenters;
  private map;
  
  ngOnInit(): void {
    this.getCenters();
  }
  constructor(private http: HttpClient, private cdr: ChangeDetectorRef, private router: Router) {}
  ngAfterViewInit(): void {}

  /**
   * Fetch Centers on Open-data API
   */
  getCenters() {
    const params = new HttpParams()
      .set("dataset", "dechetteries-en-temps-reel")
      .set("rows", "3");

    this.http
      .get("https://opendata.bordeaux-metropole.fr/api/records/1.0/search/", {
        params,
      })
      .pipe(
        tap((response: any) => (this.totalCenters = response.nhits)),
        map((response: any) => response.records.map((record) => record)),
        map((records) => {
          return records.map((record) => {
            return {
              ...record,
              acceptes: record.acceptes.split(";"),
            };
          });
        })
      )
      .subscribe((response) => {
        this.centers = response;
      });
  }

  /**
   * Add center to current detailedCenter
   * @param center
   */
  view(center) {
    this.router.navigate(['center', center.recordid]);
    this.detailedCenter = center;
    this.cdr.detectChanges();
    this.initMap();
  }

  /**
   * Define Leaflet map with options
   */
  initMap(): void {
    if (this.map) {
      this.map.remove();
    }

    this.map = L.map("map", {
      center: this.detailedCenter.geo_point_2d,
      zoom: 18,
    });

    const tiles = L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        maxZoom: 18,
        minZoom: 1,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }
    );
    const marker = L.marker(this.detailedCenter.geo_point_2d);

    tiles.addTo(this.map);
    marker.addTo(this.map);
  }
}
