import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, tap } from "rxjs";
import { HttpClient, HttpParams } from "@angular/common/http";
import { SinglecenterService } from '../../service/singlecenter.service';

@Component({
  selector: 'app-singleitem',
  templateUrl: './singleitem.component.html',
  styleUrls: ['./singleitem.component.scss']
})
export class SingleitemComponent {
  detailedCenter;
  constructor(private route: ActivatedRoute, private http: HttpClient, private centerService: SinglecenterService) {}
  ngOnInit(): void{
    this.route.params.subscribe(params => {
      const centerId = params['id'];
      // ici vous pouvez utiliser centerId pour charger les données du centre correspondant
      this.centerService.getCenter(centerId).subscribe((response) => {
        this.detailedCenter = response;
      });

    });
  }

  //récupérer les données du centre en fonction de son id
  
  
    

}
