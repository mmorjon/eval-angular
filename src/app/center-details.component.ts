import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-center-details",
  templateUrl: "./center-details.component.html",
})
export class CenterDetailsComponent implements OnInit {
  @Input() center;
  @Output() back = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}
  onBackButtonClick() {
    this.back.emit();
  }

  onBack(): void {
    this.back.emit();
  }

  getStatusColor(status: string): string {
    return status.toLowerCase() === "ouvert" ? "bg-green-200" : "bg-red-200";
  }
}
