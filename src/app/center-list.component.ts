import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map, tap } from "rxjs/operators";
import { CenterService } from "./service/center.service";

@Component({
  selector: "app-center-list",
  templateUrl: "./center-list.component.html",
})
export class CenterListComponent implements OnInit {
  @Output() view = new EventEmitter();
  centers;
  totalCenters;

  constructor(private centerService: CenterService) {}

  centersPerPage = 3;

  ngOnInit(): void {
    this.getCenters(this.centersPerPage);
  }

  getCenters(limit: number): void {
    this.centerService.getCenters(limit).subscribe((centers) => {
      this.centers = centers;
    });
  }

  getStatusColor(status: string): string {
    return status.toLowerCase() === "ouvert" ? "bg-green-500" : "bg-red-500";
  }

  onView(center) {
    this.view.emit(center);
  }
}
