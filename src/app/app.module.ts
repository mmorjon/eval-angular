import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { CenterListComponent } from "./center-list.component";
import { CenterDetailsComponent } from "./center-details.component";
import { MapDirective } from "./map/map.directive";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    CenterListComponent,
    CenterDetailsComponent,
    MapDirective,
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
