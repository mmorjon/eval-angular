import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SinglecenterService {

  constructor(private http: HttpClient) { }

  getCenter(centerId: string) {
    const params = new HttpParams()
      .set('dataset', 'dechetteries-en-temps-reel')
      .set('q', `recordid="${centerId}"`)
      .set('rows', '1');

    return this.http
      .get('https://opendata.bordeaux-metropole.fr/api/records/1.0/search/', { params })
      .pipe(
        map((response: any) => response.records[0].fields),
        map((record) => {
          return {
            ...record,
            acceptes: record.acceptes.split(';'),
          };
        })
      );
  }
}
