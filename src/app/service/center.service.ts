import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class CenterService {
  centers;
  totalCenters;
  constructor(private http: HttpClient) {}

  getCenters(limit: number = 3) {
    const params = new HttpParams()
      .set("dataset", "dechetteries-en-temps-reel")
      .set("rows", limit.toString()); // Modifiez cette ligne

    return this.http
      .get("https://opendata.bordeaux-metropole.fr/api/records/1.0/search/", {
        params,
      })
      .pipe(
        tap((response: any) => (this.totalCenters = response.nhits)),
        map((response: any) => response.records.map((record) => record.fields)),
        map((records) => {
          return records.map((record) => {
            return {
              ...record,
              acceptes: record.acceptes.split(";"),
              refuses: record.refuses.split(";"),
            };
          });
        })
      );
  }
}
