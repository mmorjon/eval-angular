import { Directive, ElementRef, Input, OnInit, OnDestroy } from "@angular/core";
import * as L from "leaflet";

@Directive({
  selector: "[appMap]",
})
export class MapDirective implements OnInit, OnDestroy {
  @Input() center: [number, number];
  private map: L.Map;

  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    if (this.center) {
      this.initMap();
    }
  }

  ngOnDestroy(): void {
    if (this.map) {
      this.map.remove();
    }
  }

  initMap(): void {
    this.map = L.map(this.el.nativeElement, {
      center: this.center,
      zoom: 18,
    });

    const tiles = L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        maxZoom: 18,
        minZoom: 1,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }
    );
    const marker = L.marker(this.center);

    tiles.addTo(this.map);
    marker.addTo(this.map);
  }
}
